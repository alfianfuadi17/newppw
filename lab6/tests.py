import time
from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest

from .models import status_model
from .views import index, home

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class Lab6UnitTest(TestCase):

    def test_lab6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_url_not_found(self):
        response = Client().get('/pepew/')
        self.assertEqual(response.status_code, 404)

    def test_lab6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_lab6_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'navi.html')
        self.assertTemplateUsed(response, 'index.html')

    def test_lab6_addstatus_using_addstatus_template(self):
        response = Client().get('/add-status/')
        self.assertRedirects(response, '/', 302)

    def test_lab6_home_using_home_template(self):
        response = Client().get('/home/')
        self.assertTemplateUsed(response, 'navi.html')
        self.assertTemplateUsed(response, 'home.html')

    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, Apa kabar?', html_response)
        
    def test_landing_homepage_is_completed(self):
        request = HttpRequest()
        response = home(request)
        html_response = response.content.decode('utf8')
        self.assertIn('ALFIAN FUADI RAFLI', html_response)
        self.assertIn('19', html_response)
        self.assertIn('082323779646', html_response)
        self.assertIn('alfianfuadi17@gmail.com', html_response)
        self.assertIn('alfianfuadi17.herokuapp.com', html_response)

    def test_can_POST_request_and_save_models(self):
        response = self.client.post('/add-status/', data = {'status' : 'coba saja'})
        counting_all_available_status = status_model.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

class Lab6FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        self.browser.implicitly_wait(25) 
        super(Lab6FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(Lab6FunctionalTest, self).tearDown()

    #HTML TEST
    def test_statuspage_html(self):
        self.browser.get('http://127.0.0.1:8000/')
        time.sleep(1.1)
        title = self.browser.find_element_by_name('titlepage')
        self.assertTrue(title.text == 'Hello, Apa kabar?')

    def test_statustable_html(self):
        self.browser.get('http://127.0.0.1:8000/')
        time.sleep(1.1)
        formBox = self.browser.find_element_by_name('status')
        submitButton = self.browser.find_element_by_name('statusbutton')
        formBox.send_keys('Coba Coba')
        submitButton.send_keys(Keys.RETURN)
        time.sleep(1.1)
        table = self.browser.find_element_by_name('statustable')
        self.assertTrue(table.text == 'Status')

    def test_timetable_html(self):
        self.browser.get('http://127.0.0.1:8000/')
        time.sleep(1.1)
        formBox = self.browser.find_element_by_name('status')
        submitButton = self.browser.find_element_by_name('statusbutton')
        formBox.send_keys('Coba Coba')
        submitButton.send_keys(Keys.RETURN)
        time.sleep(1.1)
        table = self.browser.find_element_by_name('timetable')
        self.assertTrue(table.text == 'Time')

    #CSS TEST
    def test_table_bootstrapclass(self):
        self.browser.get('http://127.0.0.1:8000/')
        table = self.browser.find_element_by_tag_name('table')
        self.assertTrue(table.get_attribute("class") == "table table-hover table-bordered table-striped")

    def test_textstyle_statuspage(self):
        self.browser.get('http://127.0.0.1:8000/')
        table = self.browser.find_element_by_name('titlepage')
        self.assertTrue(table.get_attribute("style") == "text-align: center; font-family: Consolas;")

    #SUBMIT FORM Test
    def test_submit_and_check(self):
        self.browser.get('http://127.0.0.1:8000/')
        formBox = self.browser.find_element_by_name('status')
        submitButton = self.browser.find_element_by_name('statusbutton')
        formBox.send_keys('Coba Coba')
        time.sleep(1.5)
        submitButton.send_keys(Keys.RETURN)
        time.sleep(1.5) 
        
        ada = False
        list = self.browser.find_elements_by_name('datastatus')
        for i in list:
            if (i.text == 'Coba Coba'):
                ada = True
        self.assertTrue(ada == True)
