import json
import requests
from django.shortcuts import render
from django.shortcuts import redirect
from django.http import JsonResponse
from .form import status_form
from .models import status_model

# Create your views here.
def index(request):    
    status = status_model.objects.all()
    form = status_form()
    html = 'index.html'
    return render(request, html, {'status' : status, 'form' : form})    

def home(request):
    response = {}
    return render(request, 'home.html', response)

def add_status(request):
    status = status_form(request.POST)
    if (request.method == 'POST' and status.is_valid()):
        temp = request.POST['status']
        status_model.objects.create(status=temp)

    return redirect('lab6:index')

def show_buku(request):
    return render(request, 'book.html', {});

def getdata(request):
    url = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
    jsons = url.json()
    return JsonResponse(jsons)
