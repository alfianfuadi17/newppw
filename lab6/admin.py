from django.contrib import admin
from .models import status_model

# Register your models here.
admin.site.register(status_model)