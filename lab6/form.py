from django import forms

class status_form(forms.Form):
    status = forms.CharField(label='Status:', max_length=300, required=True, 
    	widget=forms.TextInput(attrs={'class': 'form-control'}))
