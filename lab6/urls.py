from django.urls import path

from . import views

app_name = 'lab6'
urlpatterns = [
	path('', views.index, name='index'),
	path('add-status/', views.add_status, name='add_status'),
	path('home/', views.home, name='home'),
	path('getdata/', views.getdata, name='getdata'),
	path('buku/', views.show_buku, name='showbuku')
]