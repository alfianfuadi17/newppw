from django.db import models
from django.utils import timezone

# Create your models here.
class status_model(models.Model):
    status = models.CharField(max_length=300)
    timenow = models.DateTimeField(default=timezone.now, blank=True)
    
        