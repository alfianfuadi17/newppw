from django import forms
from .models import Subscriber
import datetime

class SubscriberForm(forms.Form):
	nama = forms.CharField(max_length = 40, widget = 
		forms.TextInput(attrs = {'id' : 'nama', 'class' : 'form-control', 'placeholder' : 'Nama', 'text-align' : 'center'}))
	email = forms.CharField(max_length = 40, widget = 
		forms.EmailInput(attrs = {'id' : 'email', 'class' : 'form-control', 'placeholder' : 'Email'}))
	password = forms.CharField(max_length = 40, widget = 
		forms.PasswordInput(attrs = {'id' : 'password', 'class' : 'form-control', 'placeholder' : 'Password'}))
