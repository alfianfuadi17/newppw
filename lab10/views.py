from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.db import IntegrityError
from .forms import SubscriberForm
from .models import Subscriber
import json

# Create your views here.
def landingRegisterAkun(request):
	response = {'form' : SubscriberForm}
	return render(request, "register.html", response)

def registerAkun(request):
	response = {}
	try:
	    if request.method == 'POST':
	        nama = request.POST.get('nama')
	        email = request.POST.get('email')
	        password = request.POST.get('password')
	        Subscriber.objects.create(nama=nama, email=email, password=password)
	        response['berhasil'] = True
	        response['pesan'] = 'Registrasi Berhasil'
	        json_data = json.dumps(response)
	        return HttpResponse(json_data)
	    else:
	        response['berhasil'] = False
	        response['pesan'] = 'Registrasi Gagal'
	        json_data = json.dumps(response)
	        return HttpResponse(json_data)
	except IntegrityError as e:
	    response['berhasil'] = False
	    response['pesan'] = 'Registrasi Gagal'
	    json_data = json.dumps(response)
	    return HttpResponse(json_data)
	
def validateEmail(request):
	response = {'pesan' : 'Email Valid', 'ada' : False}
	email = request.POST.get('email')
	response['ada'] = Subscriber.objects.filter(email=email).exists()
	if response['ada']:
	    response['pesan'] = 'Email Sudah Terdaftar'
	json_data = json.dumps(response)
	return HttpResponse(json_data)
