from django.urls import path
from .views import *
# Create your views here.

app_name = 'lab10'
urlpatterns = [
	path('', landingRegisterAkun, name = 'landing'),
	path('submit/', registerAkun, name = 'reg'),
	path('validate-email/', validateEmail, name = 'validate'),
]
