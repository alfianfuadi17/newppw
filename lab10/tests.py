from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest

from .models import Subscriber
from .views import *
# Create your tests here.

class Lab10Test(TestCase):	
	def test_lab10_url_is_exist1(self):
		response = Client().get('/lab10/')
		self.assertEqual(response.status_code, 200)

	def test_lab10_url_is_exist2(self):
		response = Client().get('/lab10/submit/')
		self.assertEqual(response.status_code, 200)

	def test_lab10_url_is_exist3(self):
		response = Client().get('/lab10/validate-email/')
		self.assertEqual(response.status_code, 200)

    